<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=<?php echo rand(); ?>"> -->

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery.mask.min.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/js/bootstrap.min.js">	</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/config.js">	</script>





	<?php wp_head(); ?>
	<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
        	assets: '<?php echo get_template_directory_uri(); ?>',
        	tests: {}
        });
    </script>

</head>
<body <?php body_class(); ?>>


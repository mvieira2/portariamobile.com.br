<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.4.2
 * @author     Thomas Griffin <thomasgriffinmedia.com>
 * @author     Gary Jones <gamajo.com>
 * @copyright  Copyright (c) 2014, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        // Este é um exemplo de como incluir um plug-in que já vem incluído na pasta de plugins do tema. Se quiser utilizar é só remover os comentários.
        /*array(
            'name'               => 'TGM Example Plugin', // The plugin name.
            'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),*/

        // Este é um exemplo de como incluir um plugin de um repositório privado em seu tema. Se quiser utilizar é só remover os comentários.
        /*array(
            'name'               => 'TGM New Media Plugin', // The plugin name.
            'slug'               => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
            'source'             => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
            'required'           => false, // If false, the plugin is only 'recommended' instead of required.
            'external_url'       => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
        ), */

/*
        array(
            'name'      => 'Bold Page Builder',
            'slug'      => 'bold-page-builder',
            'required'  => true,
        ),*/

        array(
            'name'      => 'Easy HTTPS Redirection',
            'slug'      => 'https-redirection',
            'required'  => true,
        ),
        
        array(
            'name'      => 'Bootstrap for Contact Form 7',
            'slug'      => 'bootstrap-for-contact-form-7',
            'required'  => true,
        ),
        
       
        array(
            'name'      => 'Elementor Page Builder',
            'slug'      => 'elementor',
            'required'  => true,
        ),
 
        array(
            'name'      => 'NavMenu Addon For Elementor',
            'slug'      => 'navmenu-addon-for-elementor',
            'required'  => true,
        ),
        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),
        array(
            'name'      => 'Smart Slider 3',
            'slug'      => 'smart-slider-3',
            'required'  => true,
        ),
        array(
            'name'      => 'Google Analytics',
            'slug'      => 'googleanalytics',
            'required'  => false,
        ),
        array(
            'name'      => 'Duplicate Post',
            'slug'      => 'duplicate-post',
            'required'  => false,
        ),

        array(
            'name'      => 'All In One Favicon',
            'slug'      => 'all-in-one-favicon',
            'required'  => false,
        ),

        // Plugin WordPress SEO é necessário | Melhor opção em SEO para WordPress
        array(
            'name'      => 'WordPress SEO by Yoast',
            'slug'      => 'wordpress-seo',
            'required'  => false,
        ),

        // Plugin W3 Total Cache é necessário | Uma das melhores opções em Cache para WordPress, muito bom para melhorar a velocidade do site.
        array(
            'name'      => 'W3 Total Cache',
            'slug'      => 'w3-total-cache',
            'required'  => false,
        ),

                                            

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Instalação de plugins necessários', 'tgmpa' ),
            'menu_title'                      => __( 'Instalar Plugins', 'tgmpa' ),
            'installing'                      => __( 'Instalando o plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Algo deu errado.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'Este tema requer o seguinte plug-in: %1$s.', 'Este tema requer os seguintes plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'Este tema requer o seguinte plug-in: %1$s.', 'Este tema recomenda/requer os seguintes plugins: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Desculpe, mas você não tem as permissões corretas para instalar o %s plugin. Contacte o administrador do site para obter ajuda sobre como tirar o plugin instalado.', 'Desculpe, mas você não tem as permissões corretas para instalar o %s plugins. Contacte o administrador do site para obter ajuda em obter os plugins instalados.' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'O seguinte plugin recomendado está atualmente desativado: %1$s.', 'Os seguintes plugins necessários são atualmente inativo: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'O seguinte plugin recomendado está desativado: %1$s.', 'Os seguintes plugins recomendados estão desativados atualmente: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Desculpe, mas você não tem as permissões corretas para ativar o %s plugin. Contacte o administrador do site para obter ajuda na obtenção do plugin ativado.', 'Desculpe, mas você não tem as permissões corretas para ativar o %s plugins. Contacte o administrador do site para obter ajuda em obter os plugins ativados.' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'O seguinte plugin precisa ser atualizado para sua versão mais recente para assegurar o máximo de compatibilidade com este tema: %1$s.', 'Os seguintes plugins precisam ser atualizados para sua última versão para assegurar o máximo de compatibilidade com este tema: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Desculpe, mas você não tem as permissões corretas para atualizar %s plugin. Contacte o administrador do site para obter ajuda sobre como tirar o plug-in atualizado.', 'Desculpe, mas você não tem as permissões corretas para atualizar o %s plugins. Contacte o administrador do site para obter ajuda em obter os plugins atualizados.' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Clique aqui e comece a instalação do(s) plugin(s)', 'Clique aqui e comece a instalação dos plugins' ),
            'activate_link'                   => _n_noop( 'Clique aqui e comece a ativação dos plugins', 'Clique aqui e comece a ativação dos plugins' ),
            'return'                          => __( 'Retornar para o painel administrativo', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin ativado com sucesso.', 'tgmpa' ),
            'complete'                        => __( 'Todos os plugins instalados e ativados com sucesso. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}
